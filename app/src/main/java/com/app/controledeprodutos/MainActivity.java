package com.app.controledeprodutos;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.tsuryo.swipeablerv.SwipeLeftRightCallback;
import com.tsuryo.swipeablerv.SwipeableRecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnClick{
    private List<Produto> produtoList = new ArrayList<>();
    private SwipeableRecyclerView rvProdutos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvProdutos = findViewById(R.id.rvProdutos);
        carregaLista();

        configRecycleView();

        imageButonOnClick();
    }


    private void imageButonOnClick(){
        ImageButton ibAdd = findViewById(R.id.ib_add);
        ImageButton ibVermais = findViewById(R.id.ib_ver_mais);

        ibAdd.setOnClickListener(view -> {
            startActivity(new Intent(this, FormProdutoActivity.class));
        });

        ibVermais.setOnClickListener(view -> {
            PopupMenu popupMenu = new PopupMenu(this, ibVermais);
            popupMenu.getMenuInflater().inflate(R.menu.menu_toobar, popupMenu.getMenu());

            popupMenu.setOnMenuItemClickListener(menuItem -> {
                if (menuItem.getItemId() == R.id.menu_sobre){
                    Toast.makeText(this, "Sobre", Toast.LENGTH_SHORT).show();
                    dialogConfig();
                }

                return true;
            });

            popupMenu.show();

        });
    }

    private void configRecycleView(){
        rvProdutos.setLayoutManager(new LinearLayoutManager(this));
        rvProdutos.setHasFixedSize(true);
        AdapterProduto adapterProduto = new AdapterProduto(produtoList, this);
        rvProdutos.setAdapter(adapterProduto);

        rvProdutos.setListener(new SwipeLeftRightCallback.Listener() {
            @Override
            public void onSwipedLeft(int position) {

            }

            @Override
            public void onSwipedRight(int position) {
                produtoList.remove(position);
                adapterProduto.notifyItemRemoved(position);
            }
        });
    }


    private void carregaLista(){

        produtoList = new ArrayList<>();

        produtoList.add(new Produto("Monitor 34 LG", 45, 1664.99));
        produtoList.add(new Produto("Caixa de Som C3 Tech", 15, 149.99));
        produtoList.add(new Produto("Microfone Blue Yeti", 38, 1699.99));
        produtoList.add(new Produto("Gabinete NZXT H440", 15, 979.99));
        produtoList.add(new Produto("Placa Mãe Asus", 60, 1199.99));
        produtoList.add(new Produto("Memória Corsair 16GB", 44, 599.99));
        produtoList.add(new Produto("Teclado Mecânico RGB", 20, 129.99));
        produtoList.add(new Produto("Mouse Gamer Logitech", 30, 79.99));
        produtoList.add(new Produto("Monitor 27 Dell", 55, 899.99));
        produtoList.add(new Produto("Headset HyperX Cloud", 25, 149.99));
        produtoList.add(new Produto("HD Externo 1TB", 10, 89.99));
        produtoList.add(new Produto("SSD Kingston 512GB", 18, 149.99));
        produtoList.add(new Produto("Cadeira Gamer", 12, 349.99));
        produtoList.add(new Produto("Webcam Logitech Full HD", 22, 79.99));
        produtoList.add(new Produto("Monitor 24 Acer", 35, 199.99));
        produtoList.add(new Produto("Mousepad XXL", 50, 29.99));
        produtoList.add(new Produto("Notebook Dell Inspiron", 8, 999.99));
        produtoList.add(new Produto("Placa de Vídeo NVIDIA RTX 3080", 5, 699.99));
        produtoList.add(new Produto("Impressora HP LaserJet", 17, 299.99));
        produtoList.add(new Produto("Roteador ASUS Wi-Fi 6", 28, 119.99));
    }

    @Override
    public void onCLickListener(Produto produto) {
        String sb = produto.getNome() + "\n" +
                "Estoque: " + produto.getEstoque();
        Toast.makeText(this, sb, Toast.LENGTH_SHORT).show();
    }

//    private void statusBarColor(){
//        Window window = this.getWindow();
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        window.setStatusBarColor(ContextCompat.getColor(this, R.color.salmao));
//    }

    private void dialogConfig(){
        Dialog dialog = new Dialog(this);
        LayoutInflater inflate = this.getLayoutInflater();
        View dialogView = inflate.inflate(R.layout.dialog_sobre, null);
        dialog.setContentView(dialogView);

        Button button = dialogView.findViewById(R.id.btn_fechar_dialog);

        Window window = dialog.getWindow();

        if (window != null){
            WindowManager.LayoutParams layoutParams = window.getAttributes();
            layoutParams.dimAmount = 0.6f;

            window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            window.setAttributes(layoutParams);
        }

        button.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }
}