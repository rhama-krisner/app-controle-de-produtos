package com.app.controledeprodutos;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.app.controledeprodutos.DAO.ProdutoDAO;

public class FormProdutoActivity extends AppCompatActivity {

    private ProdutoDAO produtoDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_produto);
        produtoDAO = new ProdutoDAO(this);
    }

    public void btnSalvar(View view){
        EditText edit_produto = findViewById(R.id.edit_produto);
        EditText edit_quantidade = findViewById(R.id.edit_quantidade);
        EditText edit_valor = findViewById(R.id.edit_valor);

        String nome = edit_produto.getText().toString().trim();
        String quantidade = edit_quantidade.getText().toString().trim();
        String valor = edit_valor.getText().toString().trim();

        if (nome.isEmpty()) {
            edit_produto.requestFocus();
            edit_produto.setError("Informe o nome do produto!");
            return;
        }

        if (quantidade.isEmpty() || Integer.parseInt(quantidade) < 1) {
            edit_quantidade.requestFocus();
            edit_quantidade.setError("Informe uma quantidade válida");
            return;
        }

        if (valor.isEmpty() || Double.parseDouble(valor) < 0.01) {
            edit_valor.requestFocus();
            edit_valor.setError("Digite um valor válido");
            return;
        }

        Produto produto = new Produto(nome, Integer.parseInt(quantidade), Double.parseDouble(valor));
        produtoDAO.salvarProduto(produto);

        limpaEditText();
    }

    private void limpaEditText(){
        EditText nome = findViewById(R.id.edit_produto);
        EditText quantidade = findViewById(R.id.edit_quantidade);
        EditText valor = findViewById(R.id.edit_valor);

        nome.setText("");
        quantidade.setText("");
        valor.setText("");
    }

}