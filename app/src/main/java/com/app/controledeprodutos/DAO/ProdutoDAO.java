package com.app.controledeprodutos.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.app.controledeprodutos.Produto;
import com.app.controledeprodutos.db.DBHelper;

public class ProdutoDAO {
    private final SQLiteDatabase write;
    private final SQLiteDatabase read;

    public ProdutoDAO(Context context) {
        DBHelper dbHelper = new DBHelper(context);
        this.write = dbHelper.getWritableDatabase();
        this.read = dbHelper.getReadableDatabase();
    }

    public void salvarProduto(Produto produto){
        ContentValues contentValues = new ContentValues();
        contentValues.put("nome", produto.getNome());
        contentValues.put("estoque", produto.getEstoque());
        contentValues.put("valor", produto.getValor());

        try {
            write.insert(DBHelper.tb_produto(), null, contentValues);
            //write.close();
        } catch (Exception e){
            Log.i("Error", "Erro ao salvar produto: " + e.getMessage());
        }
    }

}
